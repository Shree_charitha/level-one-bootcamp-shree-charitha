#include<stdio.h>
#include<math.h>
struct points
{
float x, y;
};
typedef struct points Point;
float distance(struct points a, struct points b){
float result;
result = sqrt((a.x - b.x) * (a.x - b.x) + (a.y - b.y) *(a.y - b.y));
return result;
}
Point input(){
Point p;
printf("Enter the coordinates of point A\n");
printf("X - axis coordinate:\n");
scanf("%f", &p.x);
printf("Y - axis Coordinate:\n");
scanf("%f", &p.y);
return p;
}
void display(float a, float b)
{
printf("Distance between points A and B: \n%f", distance(a, b));
}
int main()
{
struct points a,b,s;
a=input();
b= input();
display(a,b);
return 0;
}
//WAP to find the distance between two points using structures and 4 functions.